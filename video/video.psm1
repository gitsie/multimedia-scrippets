$Global:isoDate = Get-Date -Format yyyyMMdd
<#
.Synopsis
   Builds an empty directory structure, underneath a directory named after the current date in ISO format
.DESCRIPTION
   Long description
.EXAMPLE
   Build-DirectoryStructure
.EXAMPLE
   Build-DirectoryStructure -SourceDir 'D:\Source' -TargetDir '\\server\share\target'
#>

function Build-DirectoryStructure
{
    [CmdletBinding()]
    [Alias()]
    [OutputType([int])]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $SourceDir = 'E:\',

        # Param2 help description
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $TargetDir = '\\nas.hivos.nl\share$'
    )

        $robocopy = 'c:\windows\system32\robocopy.exe'
	    $Exclude = 'E:\IT E:\Applications E:\Bucket "E:\`$RECYCLE.BIN" "E:\System Volume Information"'
        $TargetDir = $TargetDir  + "\" + $isoDate
        New-Item $TargetDir -Type Directory 
        Invoke-Expression "$robocopy $SourceDir $TargetDir /E /XF * /XD $Exclude"
}


<#
.Synopsis
   Short description
.DESCRIPTION
   Long description
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
.Notes
Stolen/adapted from original script by Sam Boutros
#>


function Get-Files {

[CmdletBinding(SupportsShouldProcess=$true,ConfirmImpact='Low')] 
    Param(
        [Parameter(Mandatory=$true,
                   ValueFromPipeLine=$true,
                   ValueFromPipeLineByPropertyName=$true,
                   Position=0)]
            [String[]]$TargetFolders, 
        [Parameter(Mandatory=$false,
                   ValueFromPipeLine=$true,
                   ValueFromPipeLineByPropertyName=$true,
                   Position=1)]
            [String]$CsvFile='c:\temp\mediafiles_output.csv', 
        [Parameter(Mandatory=$false,
                   Position=2)]
            [String[]]$FileTypes = "*", 
        [Parameter(Mandatory=$false,
                   Position=3)]
            [ValidateSet("Name","DirectoryName","Extension","Length","CreationTime","LastAccessTime","LastWriteTime","Attributes")]
            [Array]$Properties = @("Name","DirectoryName","Extension","Length","CreationTime","LastAccessTime","LastWriteTime","Attributes"), 
        [Parameter(Mandatory=$false,
                   Position=4)]
            [Long]$MinFileSize = 0 , 
        [Parameter(Mandatory=$false,
                   Position=5)]
            [ValidateSet("Byte","KB","MB","GB","TB")]
            [String]$SizeUnit = "Byte"
    )

Remove-Item $CsvFile -Force -Confirm:$false

$TargetFoldersString = $null   
        foreach ($TargetFolder in $TargetFolders) {
            $TargetFoldersString += "$TargetFolder, "
            $FileTypesString = $null
            foreach ($FileType in $FileTypes) {
                $FileTypesString += "$FileType , "
                $Files = Get-ChildItem -Path $TargetFolder -Include *.$FileType -Force -Recurse -ErrorAction SilentlyContinue
                $FileList = @()
                ForEach ($File in $Files) {
                    if ($File.Length -gt $MinFileSize) {
                        $MyObjProps = @{}
                        foreach ($Prop in $Properties) { $MyObjProps.Add($Prop, $($File.$Prop)) } 
                        if ($MyObjProps.Get_Item("Length")) {
                            switch ($SizeUnit.ToUpper()) {
                                "KB" {$MyObjProps.Set_Item("Length", "{0:N2}" -f ($File.Length/1KB) )}
                                "MB" {$MyObjProps.Set_Item("Length", "{0:N2}" -f ($File.Length/1MB) )}
                                "GB" {$MyObjProps.Set_Item("Length", "{0:N2}" -f ($File.Length/1GB) )}
                                "TB" {$MyObjProps.Set_Item("Length", "{0:N2}" -f ($File.Length/1TB) )}
                            }
                        }
                        $MyFile = New-Object -TypeName PSObject -Property $MyObjProps
                        $FileList += $MyFile
                    }
                } 
                if ($FileList.Count -gt 0) { # Skip empty tables
                    $FileList |Export-Csv $CsvFile -Append -NoTypeInformation
                } else {
                    Write-Host "  No matching files found"
                }
            } # File Types
       }
}



<#
.Synopsis
   Short description
.DESCRIPTION
   Long description
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
#>

function Convert-Movie
{
    [CmdletBinding()]
    [Alias()]
    [OutputType([int])]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [string]$handbrakecli="c:\Program Files\Handbrake\handbrakecli.exe",

        [Parameter(Mandatory=$false)]
        [string]$directory,

        [Parameter(Mandatory=$false)]
        $flag = $directory + '\conversion-finished.end',

        # Param2 help description
        [Parameter(Mandatory=$false)]
        [string]$extension=".mp4"
    )

    Begin
    {
            If (-not(Test-Path -Path $directory)){
                $directory = '\\nas.hivos.nl\share$\' + $isoDate
            }

            If (Test-Path -Path $flag) {
                Exit
            }
    }
    Process
    {
        foreach ($file in $(Get-ChildItem -Path $directory -Recurse -Include *.* -Exclude *_converted_*)) {
            $input = $file.FullName
            $output = $file.DirectoryName + '\' +  $file.BaseName + "_converted_" + $extension
            $log = $file.DirectoryName + '\' + $file.BaseName + ".log"
            $Parameters =  " -i `"$input`" -o `"$output`" -Z `"Fast 1080p30`""
            $Prms = $Parameters.Split(" ")
            & "$handbrakecli" + $Prms 2>&1 | Out-File $log 
       }
    }
    End
    {
        New-Item -Path $flag -ItemType File 
    }
}

function Move-Files
{
    [CmdletBinding()]
    [Alias()]
    [OutputType([int])]
    Param
    (
	[Parameter(Mandatory=$false,
        	ValueFromPipeLine=$true,
	        ValueFromPipeLineByPropertyName=$true,
        	Position=0)]
        [String]$CsvFile='c:\temp\mediafiles_output.csv', 
	
	[Parameter(Mandatory=$false,
        	ValueFromPipeLine=$true,
	        ValueFromPipeLineByPropertyName=$true,
        	Position=1)]
        [String]$LogFileDir='c:\temp\', 
        
	# Param2 help description
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=2)]
        $TargetDir = '\\nas.hivos.nl\share$',

	# Param2 help description
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=3)]
        [Switch]$FromNasToFiler
    )
    
    $robocopy = 'c:\windows\system32\robocopy.exe'
    
    $paths = Import-Csv $CsvFile
	If ($FromNasToFiler) {
      	foreach ($path in $paths) {
	        $LogFile = $LogFileDir + $isoDate + " - FromNasToFiler.log"
            Remove-Item $LogFile -Force -Confirm:$false
		    $nasPath = ($path.DirectoryName).substring(3)
		    $nasPath = $TargetDir + "\" + $isoDate + "\" + $nasPath
            $convertedFile = $path.Name -replace ".{4}$", "_converted_.mp4"
		    Invoke-Expression '& $robocopy $nasPath $path.DirectoryName /MOV /R:3 /W:1 /Z $convertedFile /NP /LOG+:$LogFile'
		    }
        }
	Else {
	    foreach ($path in $paths) {
	    	$LogFile = $LogFileDir + $isoDate + " - FromFilerToNas.log"
		    Remove-Item $LogFile -Force -Confirm:$false
            $nasPath = ($path.DirectoryName).substring(3)
		    $nasPath = $TargetDir + "\" + $isoDate + "\" + $nasPath
		    Invoke-Expression '& $robocopy $path.DirectoryName $nasPath /MOV /R:3 /W:1 /Z $path.Name /NP /LOG+:$LogFile'
            }
        }
}