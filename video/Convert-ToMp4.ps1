$handbrakecli="c:\Program Files\Handbrake\handbrakecli.exe"

$textfile = Get-Content c:\tmp\path.txt
foreach ($line in $textfile) {
	$output = $line -replace ".mov", ".mp4"
	write-output $line
	write-output $output
	Invoke-Expression '& $handbrakecli -Z "Fast 1080p30" -i "$line" -o "$output"'
}